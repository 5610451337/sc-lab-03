package controller;

import model.Hashing;
import view.GUI;

public class Testcase {
	public static void main(String[] args){
		new Testcase();
	}
	private GUI frame;
	private Hashing hash;

	public Testcase() {
		createFrame();
		setTestcase();
	}
	public void createFrame(){
		frame = new GUI();
		frame.setVisible(true);
		frame.setLocation(250,150);
		frame.setSize(800, 450);	
	}
	public void setTestcase(){
		hash = new Hashing();
		String url1 = " http://www.cs.sci.ku.ac.th/~fscichj/ ";
		String url2 = " www.facebook.com ";
		String url3 = " www.twitter.com ";
		String url4 = " www.google.co.th ";
		String url5 = " www.blognone.com ";
		String url6 = " www.bitbucket.com ";
		String url7 = " www.edmodo.com ";
		String url8 = " www.youtube.com  ";
		String url9 = " www.pantip.com ";
		String url10 = " www.hotmail.com ";
		String url11 = " www.ku.ac.th ";
		String url12 = " www.eclipse.org ";
		
		frame.setResult(url1+" : "+hash.modnum(hash.Hash(url1)));
		frame.setResult(url2+" : "+hash.modnum(hash.Hash(url2)));
		frame.setResult(url3+" : "+hash.modnum(hash.Hash(url3)));
		frame.setResult(url4+" : "+hash.modnum(hash.Hash(url4)));
		frame.setResult(url5+" : "+hash.modnum(hash.Hash(url5)));
		frame.setResult(url6+" : "+hash.modnum(hash.Hash(url6)));
		frame.setResult(url7+" : "+hash.modnum(hash.Hash(url7)));
		frame.setResult(url8+" : "+hash.modnum(hash.Hash(url8)));
		frame.setResult(url9+" : "+hash.modnum(hash.Hash(url9)));
		frame.setResult(url10+" : "+hash.modnum(hash.Hash(url10)));
		frame.setResult(url11+" : "+hash.modnum(hash.Hash(url1)));
		frame.setResult(url12+" : "+hash.modnum(hash.Hash(url2)));
	}

}

