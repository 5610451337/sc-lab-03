package model;

public class Hashing {
	public long Hash(String str){
		long numAscii = 0;
		for(int i=0;i<str.length();i++){
			numAscii = numAscii+(int) str.charAt(i);
		}
		return numAscii;
	}
	public long modnum(long numAscii){
		long modnum = 0;
		modnum = numAscii%4;
		return modnum;
	}

}
