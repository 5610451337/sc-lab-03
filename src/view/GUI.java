package view;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class GUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea resultsArea;
	
	public GUI() {
		// TODO Auto-generated constructor stub
		createPanal();
	}
	public void createPanal(){
		resultsArea = new JTextArea("----Results----");
		add(resultsArea);
	}
	public void setResult(String str){
		String str1 = resultsArea.getText();
		resultsArea.setText(str1+"\n"+str);
	}

}
